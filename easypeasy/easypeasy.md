easypeasymethod.org is a free and open source method of quitting pornography without willpower, any sense of deprivation, or sacrifice.

It works different from all other methods of quitting pornography because it doesn't use willpower -- anything involving counting, blocking, restricting, rewarding or punishing yourself for using pornography.

Instead, people deconstruct the reasons they're using pornography in the first place, which enables them to quit with elation -- as if being cured of a terrible disease -- as porn is illustrated to have zero benefits in the face of a escalating withdrawal.

Most people have found the other projects through this one, as it's grown to 70,000 people monthly.

Current priorities for easypeasy are:

- rewrite
- video guide
- audiobook
- relapse guide
- computer addiction book.