

easypeasymethod.org ➡️ no nut november ➡️ calm college campuses ➡️ local communities



**easypeasymethod.org** is rather popular (70,000/mo) and very effective
now, the audience is 
- creating grassroots online content for exponential growth
- placing posters and letters locally
- cultivating purposeful calmness.



**'eternal no nut november'** is the catalyst for driving people to urbit
creating a calm foundation using
- critical thinking
- healthy eating
- learning.



then, cultivating **calm university campuses globally**, united under values:
- vibe
	meeting up in parks, hanging with mates, socialising
- grow
	locally growing potatoes (and more), which are cheaper, healthier and more sustainable than ramen
- calm
	living undistracted, critically thinking, supporting others
- chill
	
- local
	finding ways to make the wider community a better place.




finally, spreading this cultivated change to local communities with meetups and coordinated action.
in the interim, people can get involved by 
- growing their own food
- volunteering 
- living undistracted
- cultivating good habits
- socialising.






if you're keen, please introduce yourself in the hello channel -- let's do this thing
there is only we