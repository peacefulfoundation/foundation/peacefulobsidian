


[[euphoriaer]]

[[journeyer]]

[[adventurer]]


[[sharer]]

[[enjoyer]]

The following is formatted in a markdown table that describes the intersection of the availability and role.

|                | **[[computers]]** | **[[programming]]** | **[[organising]]** | **[[influencing]]** | **[[research]]** | **[[editing]]** | **[[artistry]]** |
| -------------- | ----------------- | ------------------- | ------------------ | ------------------- | ---------------- | --------------- | ---------------- |
| **[[euphoriaer]]** | [[outside]]       | [[design]]          | [[intersect]]      | [[culture]]         | [[solve]]        | [[strategy]]    | [[strategy]]     |
| **[[journeyer]]**  | [[bring]]         | [[make]]            | [[allocate]]       | [[country]]         | [[understand]]   |                 |                  |
| **[[adventurer]]** | [[together]]      | [[build]]           | [[sort]]           | [[kewl]]            | [[consider]]     |                 |                  |
| **[[sharer]]**     |                   |                     |                    |                     |                  |                 |                  |
| **[[enjoyer]]**    |                   |                     |                    |                     |                  |                 |                  |
