
Hey $USER_NAME,

Thanks for joining. We're running local, social and global campaigns to kill the little monster, together. 
Here's a video on how we're doing it. 
https://youtube.com/WATCHLINK

Here's the core tasks you can do to help.
(check them off to go to the next core task!)

1. #shareyourstory on social media
   :tick: question:
2. Send letters to religious organisations in your local community. 
   :tick: :question:
3. Chat in #regional_channel
   
You selecting $AVAILABILITY and $ROLE gives you the $MATRIX direction.
https://publish.obsidian.md/quiteasily/roles/$ROLE/$MATRIX
:tick:

Here's an AI generated task for you to do.

$AI_GENERATED_TASK

You can also respond using natural language with any questions, ideas or to refine your task.