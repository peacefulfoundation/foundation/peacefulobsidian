
Primary focus: [[quiteasily]]

Secondary focus: [[reasonablediet]]






## quiteasily

- guides for online impact have been given at https://quiteasily.org/help
- guides for local impact have been given at https://quiteasily.org/local

## calm.college

- database of all university campuses around the globe
- map of all university campuses
- guide for making events
- working group on which social problems to target in big database
- research to find statistics for poverty rates, etc.

## reasonable.diet

- create https://based.cooking clone for recipes
- create recipes for recipe book