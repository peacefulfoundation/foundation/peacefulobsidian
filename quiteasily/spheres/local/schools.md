

Hello,


A worldwide campaign about stopping pornography without using willpower and completely, no longer having any desire for using it.
Completely, having recognised that pornography is simply a strangely stigmatised addictive substance that everyone is exposed to and then regards as normal; and are inspired on taking action against it within their local communities -- it's the reason you're reading this letter now.

Obviously when you read the word pornography a whole bunch of challenges appear about discussing it, even though everybody knows they're using it. 

Parents themselves should be warned about the dangers and inherent risks of pornography, and should be trained in discussing it without judging or stigmatising their children.

Schools should be able to openly discuss the very clear impacts that pornography is having on their students, but decoupled from notions of being progressive or even political.

We would think that lung cancer was normal adolescence if smoking was starting from age eight as well, and the second-hand smoke back then was dreadful too, and really, the serious intergenerational impacts as well.



Curing this is relatively easy, the entire campaign is essentially chain mail for creating conversations about pornography on a local, social and global level.

It's the reason why you're reading this letter now: there's a completely creative commons book about quitting pornography without willpower online, and we wanted to ask if you'd take some small actions to effectively combat pornography.

Such as,
- Expressing concern to governance boards
- Discussing with other education professionals online
- Recognising the signs of supernormal stimulus progressively desensitising any users' brain, such as any use of it. [1]  [2]  [3]

You're also welcome just to #shareyourstory online. I'm sure that parents would like to know the lexicon.

I think you'll find your students mental health rapidly improving over the next couple of months, as quitting pornography and not being addicted more generally becomes the norm. My focus is inspiring them to make the world a more local and peaceful place, and I'm asking that you encourage this in whatever ways you can.

Key to it is ensuring that students take actionable and measurable steps to reduce a chosen statistic, and not just succumbing to protests or awareness campaigns.

You might find that a bored enough student would conduct self directed learning if they had a real passion and their thoughtspeed was low enough; and you might like teaching things that you love to students with hope.
Calmer and happier teachers create better students and a more harmonious world, and ending pornography is a simple method of grooving to it.



Again, students are responsibly discussing pornography already, let's start doing it too.

Thank you for your consideration,
peacefulfoundation.org