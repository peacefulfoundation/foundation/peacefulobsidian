# Physical Toolkit

Hello,

Local impact is the most important part. Here's how to do it.
Print this out and follow all instructions.

------
**expires 3/7/2024 -- do not follow this guide after this date**
go to https://quiteasily.org/local to know what to do
*why? our campaign is constantly evolving, and we don't want to double up on impact*

**now**

[ ] I sat in a dark room for six hours without distractions and thought about life

[ ] I followed the reset protocol from https://reasonable.diet

[ ] I printed out this checklist.

**local**

[ ]search for different organisations in your area on maps, and make a list below

----------------
*churches, mosques, ________, ________* here






----------------

[ ] I found a printer to use at _______________________.
*home, work, school, university, library, copy shop, grandparents


[ ] Print out letters from https://quiteasily.org/letters

__ x church letter(s) [ ] 

__ x mosque letter(s) [ ]

__ x ______ letter(s) [ ] 

Then, give these letters to the places you listed.


[ ] Print out posters from https://quiteasily.org/local

I've printed __ posters to and put them around ____________ [ ] 



[ ] I videoed myself doing these tasks (even anonymously) and posted them to social media with #quiteasily and #shareyourstory (optional)


[ ] I've maked my area done on https://quiteasily.org/map

PAGE TWO

**Further**

Okay, now consider spaces where you could share addiction recovery resources.
These could be group chats, online communities, and local organisations.
Make your own checklists below.

------------------







------------------

To impact schools, find an expert speaking out against pornography in your country, then send the 'experts letter' from https://quiteasily.org/local to them.

[ ] I sent a letter to ________________________

Then, catalog this person on quiteasily.org/map





