understanding cultures and inspiring change


## consider these

finding,
- celebrities
- influencers
- journalists
- [[not for profit organisations]]
- examples of people speaking out against pornography
and infinitely more.

## actions

1. creating customised approaches for bringing people onboard and inspiring their audience
2. considering relevant events to bring media attention to a stigmatised issue
3. finding celebrities who could speak out against porn whilst mass action is occuring, and considering how to market it to them
4. finding influencers on a country or regional basis, and figuring out how to market it to them
5. customising approaches for outreach to not for profit organisations

## looping

there's a bunch of different projects, so consider the wide range of peaceful foundation initiatives that different demographics of your country would be interested in.
from there, you need to consider the most effective ways of reaching out to them.

for instance, the most effective way of reaching out to all the churches in a country is probably the head of the religious organisation, but the same thing applies for reaching out to any 'tree diagram' organisation.
but you want to consider both a bottom up, and a top down way of approaching the issue.

when I say understanding a culture, we need to unite a bunch of people (university students, school students, religious leaders, elderly people, and many others) for common goals against pornography, and then later, easy ways of improving the world around them -- a culture, so your approach will look different depending on stigma and how your society achieves cohesion generally.

