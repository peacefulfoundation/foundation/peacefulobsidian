consider everyone on the project and combine their skills together effectively

## consider these

- the different projects we're working on
- having the least amount of overhead and friction possible
- using automatic systems when possible so that everyone knows what to do.
## actions

1. looking upon this little chart and consider how a working group of people could work together on a new initiative that would make the world a better place, giving each person a little mission
2. making sure that everyone who wants a task, gets one -- which adapts to where we are in the project
3. creating tasks and little challenges to direct people into making an impact, mainly locally and socially


## looping

there's people with different skills, in different places around the world -- your goal is helping them to intersect both online and locally, in different skillsets.

there's many different tasks making small teams work on an aspect of the project, but you need to make sure that if you're going to create a team that the approach you're taking actually makes one of the [[research]] metrics move.