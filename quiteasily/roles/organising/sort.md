Everyone arriving should be doing something they find purposeful and fun.


## consider these

- projects that peaceful foundation is working on
- what brings people joy in doing
- how much time people have to commit.

## actions

1. Chatting with people in the welcome channel and get to know then
2. Introducing people to other people in the organisation
3. Getting people involved in their local communities

## looping

there's no shortage of people arriving to help, and we don't want them to be confused on how they can.
also, we want to make sure that people are going outside and feeling sunshine instead of just spending time online.

we're breaking the habits that an addiction and formed in it's wake.


so when someone arrives, you want to make sure they're impacting the world around them in a positive way, and if they have specialised skills, or interest in helping, clear direction in making this so.

what it will probably look like is welcome teams and introducing people, and as an auxiliary, being part of another team working on something