create things for working together online


## consider these

- the many different different people we want to reach out to (churches, mosques, addiction recovery groups, subject matter experts)
- the most effective ways of reaching out to them, such as to interfaith groups
- making the process of reaching out to people fun, and showing clear progress being made.


## actions

1. spreadsheets and discord bots to easily show people what they can do, and our overall progress
2. bringing people in similar locations together to solve problems
3. collating easy to understand metrics for different issues (poverty, addiction, homelessness) in their local communities
4. providing active citizens easy to understand plans to solve these problems
5. making tackling these challenges as smooth as possible

(see [[computers]] for things that need to be created)


## looping

the key thing is making sure that people are making measurable change.
for each issue, make people aware of a "god metric" like the actual number of people experiencing homelessless, and get people focused on the issue to put it on the wall.

for example, my metric for easypeasy is "number of sexual assault cases" -- because desexualising a society will make this number go down.
if I focused on pageviews or something like that, I'm not building my metric around actual change.

so, your job is to find all the problems in the world and find ways of measuring them into a single metric.