
since the best thing you can do with your computer is turning it off -- sorry.
find where people are around the world and get them together working on [[_organisation/roles/influencing|influencing]] within their [[_archive/_project/local|local]] communities

## consider these

- different organisations in your local community that would find easypeasy useful
- the problems facing your local community in general
- different initiatives that people here, and peaceful foundation is doing that could help the world.

## actions

1. finding people who could signal boost easypeasy in your local community
2. meeting other active citizens and bringing them together
3. running a meetup in a local space to bring communities together
4. adapting a peaceful foundation initiative so that it works for your local community
5. considering what people in your local community are wanting.


## looping

you want to inspire people to create local groups for the other peaceful foundation projects, whilst getting experience doing so yourself in your local communities. 
you can do online stuff everywhere else here, but it's important we're making a real difference on the world -- nothing else works.