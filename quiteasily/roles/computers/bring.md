more people into easypeasy

this is mostly distributed communities, like discord servers and such.

## consider these

- your personal influence and reach within online spaces
	- discord servers that you're part of (self improvement, success, or even ones not traditionally against porn)
	- spaces that you moderate and could easily propose partnering with
	- spaces where you have sway with admins, or they're reachable to propose signal boosting
- the influence of people you know, and how you might help them
- people you follow with large communities.

## actions

1. change your discord status
2. reach out to admins and people with sway
3. discuss pornography and say how surprisingly good being free feels
4. find more spaces where you can encourage easypeasy to be promoted
5. get admins on board using your [[easypeasy/with/passport]].


## looping

you can reach out to different online communities and make them aware of easypeasy, and adapt and work together on using their community to make a difference

for instance, reaching out to different content creators communities means meeting with the person in charge and getting them on board with discussing a normalised addiction. the creator might have already done so.
however, it's about giving their community purpose and vibing together, and the creator might find that cool too