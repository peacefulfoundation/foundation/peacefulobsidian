elegant software to solve widespread social problems, together


## consider these

- friction within any process that we're doing
- does the process actually need to take place, or are we just creating busywork?
- what are the most effective processes to create the most social impact?

## actions

1. designing and developing elegant software to solve problems we're facing; see [[programming]]
2. reducing roadblocks and increasing clarity automatically
3. looking at the [[intersect]] of different people working together, and helping them work more effectively
4. write specifications for elegantly simple software
5. design pleasant and incredibly simple user interfaces

## looping

many people want to make the world a better place, and your goal is making doing that as easy as possible.
it doesn't have to look pretty to start off with -- and this also keeps it authentic -- but the process of creating impact should be as simple as possible.

it's the intersects; someone on this chart could work with someone else to create something beautiful, and your job is to design (and, also, create) the tools that allow them to do it. such is the responsibility of noticing and thinking; there's a lot of people, processes and local problems to be worked on


