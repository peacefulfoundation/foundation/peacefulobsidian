
On the X axis is the amount of time that people have available to volunteer and focus on peaceful foundation projects.

Such projects should be fun, and should adapt around you.
We're all in this together, so please don't worry about the specific time role someone has -- anyone can do any task, no matter how much time they have free.

As Cal Newport says, the formula is
	```work completed = intensity of focus * time```

So it's really just focusing on what needs to get done for the project, at that moment in time.


Personally, I can spend an entire day deeply working on various things for the project, but I have to be honest with myself and take a break when I feel my focus waning.
That flow state is the fun part, and that fun is why I can stay in it all day.