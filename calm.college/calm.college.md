
calm.college is for creating better university campuses.
communities are their collective thoughts

university students will likely start eating [[reasonablediet]] and then the question becomes "what else can we get them to do?"

the rationale goes that every university student wants interdisciplinary collaboration, but universities don't do it because it's difficult to mark, so we might as well do it ourselves.

The website contains a brief manifesto for university campuses, guides on setting up events for people to chill and mingle, and databases of statistics of social problems in local areas.

