peaceful foundation is the parent organisation of all other projects, with the ethos:
"You can't blueprint society.
-ism's or ideologies will always fall short because of the world's complexity.
We've got to work together, learning and adapting as we go.
Let's create something peaceful to build upon."